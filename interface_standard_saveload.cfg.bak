///////////////////////////////////////////////////////////////////////////////
//
// Copyright 1997-2000 Pandemic Studios, Dark Reign II
//

ConfigureInterface()
{
  DefineControlType("Class::SaveLoad", "Game::SaveLoad")
  {
    CreateVarInteger("quick");

    CreateControl("SlotListTitle", "Static")
    {
      Font("System");
      Pos(10, 10);
      Size(500, 40);
      Style("Transparent");
      JustifyText("Left");
      Text("#shell.win.options.saveload.title");
    }

    CreateControl("List", "ListBox")
    {
      ReadTemplate("Std::Client");
      Pos(10, 60);
      Size(950, 464);
      UseVar("$<.slotName");
      Style("!VSlider");
      ItemConfig()
      {
        Font("Console");
        Geometry("ParentWidth");
        Size(0, 44);
      }

      AddTextItem("Slot0");
      AddTextItem("Slot1");
      AddTextItem("Slot2");
      AddTextItem("Slot3");
      AddTextItem("Slot4");
      AddTextItem("Slot5");
      AddTextItem("Slot6");
      AddTextItem("Slot7");
      AddTextItem("Slot8");
      AddTextItem("Slot9");

      TranslateEvent("ListBox::SelChange", "Game::SaveLoad::Message::Select", "<");
    }

    CreateControl("Save", "Button")
    {
      ReadTemplate("Std::Button");
      Style("NoAutoActivate");
      Geometry("Bottom");
      Pos(10, -10);
      Size(160, 50);
      Font("Button");
      Text("#standard.buttons.save");
      OnEvent("Button::Notify::Pressed")
      {
        SendNotifyEvent("<", "Game::SaveLoad::Message::SaveRequest");
      }
    }

    CreateControl("Load", "Button")
    {
      ReadTemplate("Std::Button");
      Geometry("Bottom", "Right");
      Size(160, 50);
      Pos(-10, -10);
      Font("Button");
      Text("#standard.buttons.load");
      OnEvent("Button::Notify::Pressed")
      {
        SendNotifyEvent("<", "Game::SaveLoad::Message::LoadRequest");
      }
    }

    CreateControl("Saving", "Window")
    {
      ReadTemplate("Std::WindowNoTitle");
      Style("AdjustWindow", "NoAutoActivate");
      Size(400, 200);
      Geometry("HCentre", "VCentre");

      CreateControl("Wait", "Static")
      {
        Geometry("ParentWidth", "ParentHeight");
        Style("Transparent");
        Font("System");
        Text("#shell.win.options.saveload.wait");
      }
    }

    CreateControl("Describe", "Window")
    {
      ReadTemplate("Std::Window");
      Geometry("ParentWidth", "HCentre", "VCentre");
      Style("Modal", "NoAutoActivate");
      Size(-100, 200);

      CreateControl("Edit", "Edit")
      {
        ReadTemplate("Std::Client");
        Geometry("ParentWidth", "HCentre");
        Pos(0, 20);
        Size(-40, 44);
        MaxLength(64);
        Font("System");
        UseVar("$<<.description");
        NotifyParent("Edit::Notify::Entered", "Described");
      }

      CreateControl("Done", "Button")
      {
        ReadTemplate("Std::Button");
        Geometry("Bottom", "Right");
        Pos(-10, -10);
        Size(160, 50);
        Font("Button");
        Text("#standard.buttons.ok");
        TranslateEvent("Button::Notify::Pressed", "Described", "<");
      }

      CreateControl("Cancel", "Button")
      {
        ReadTemplate("Std::Button");
        Align("^");
        Geometry("VInternal");
        Pos(-10, 0);
        Size(160, 50);
        Font("Button");
        Text("#standard.buttons.cancel");
        OnEvent("Button::Notify::Pressed")
        {
          Deactivate("<");
        }
      }

      OnEvent("Described")
      {
        SendNotifyEvent("<", "SaveLoad::SaveNow");
        Deactivate();
      }
    }

    OnEvent("QuickSave")
    {
      Op("$.quick", "=", 1);
      SendNotifyEvent("", "Game::SaveLoad::Message::SaveRequest");
    }

    OnEvent("Game::SaveLoad::Notify::SaveAvailable")
    {
      Activate("Save");
    }

    OnEvent("Game::SaveLoad::Notify::SaveUnavailable")
    {
      Deactivate("Save");
    }

    OnEvent("Game::SaveLoad::Notify::SelectedUsed")
    {
      Enable("Load");
    }

    OnEvent("Game::SaveLoad::Notify::SelectedFree")
    {
      Disable("Load");
    }

    OnEvent("Game::SaveLoad::Notify::SaveSlotFree")
    {
      SendNotifyEvent("", "SaveLoad::SaveDescribe");
    }

    OnEvent("Game::SaveLoad::Notify::SaveSlotUsed")
    {
      If("$.quick")
      {
        MessageBox()
        {
          Title("#shell.win.options.saveload.confirm");
          Message("#shell.win.options.saveload.confirmsave");
          Button0("#standard.buttons.ok", "SaveLoad::SaveNow");
          Button1("#standard.buttons.cancel", "");
        }
      }
      Else()
      {
        SendNotifyEvent("", "SaveLoad::SaveDescribe");
      }
    }

    OnEvent("SaveLoad::SaveDescribe")
    {
      If("$.quick")
      {
        SendNotifyEvent("", "SaveLoad::SaveNow");
      }
      Else()
      {
        Activate("Describe");
      }
    }

    OnEvent("SaveLoad::SaveNow")
    {
      Activate("Saving");
      SendNotifyEvent("", "Game::SaveLoad::Message::SaveCycle");
    }

    OnEvent("Game::SaveLoad::Notify::SaveProceed")
    {
      SendNotifyEvent("", "Game::SaveLoad::Message::Save");
    }

    OnEvent("Game::SaveLoad::Notify::SaveEnd")
    {
      Deactivate("Saving");

      If("$.quick")
      {
        Deactivate("<<");
      }
    }

    OnEvent("QuickLoad")
    {
      SendNotifyEvent("", "Game::SaveLoad::Message::LoadRequest");
    }

    OnEvent("Game::SaveLoad::Notify::LoadProceed")
    {
      SendNotifyEvent("", "SaveLoad::LoadNow");
    }

    OnEvent("Game::SaveLoad::Notify::LoadConfirm")
    {
      MessageBox()
      {
        Title("#shell.win.options.saveload.confirm");
        Message("#shell.win.options.saveload.confirmload");
        Button0("#standard.buttons.ok", "SaveLoad::LoadNow");
        Button1("#standard.buttons.cancel", "");
      }
    }

    OnEvent("SaveLoad::LoadNow")
    {
      SendNotifyEvent("", "Game::SaveLoad::Message::Load");
    }

    OnEvent("Window::Notify::Deactivated")
    {
      Op("$.quick", "=", 0);
    }
  }
}