# Dark Reign 2 - 4X Bigger Interface

A mod that enlarges as much of the GUI as possible by 4X. Built for 1440p and 4K.

Vanilla
![](https://gitlab.com/xyifer12/dark-reign-2-4x-bigger-interface/-/raw/master/ImagesAndMisc/Vanilla.png)

Modified
![](https://gitlab.com/xyifer12/dark-reign-2-4x-bigger-interface/-/raw/master/ImagesAndMisc/Modif.png)

To install, move 4xBiggerInterface.pak into your `Dark Reign 2\mods\addon` directory.
